// This is the classic Guess my Number game. The program randomly generates a number between 1 and 10 and the user tries to guess it until they get it correct.

class Guessmynumber {
    static function main(){
        var unguessed = true;
        var num = Std.random(10);
        trace("I'm thinking of a number between 1 and 10, guess what it is!");
        while (unguessed == true){
            var guess = Sys.stdin().readLine();
            if (Std.parseInt(guess) == num){
                unguessed = false;
                trace('You win! My number was ${num}.');
            }else{
                trace("Nope, guess again...");
            }
        }
    }
}
