// This program gets input from the user, if they input 0, 0 will be output and the program will be ended. If they input 1, the program outputs 1 infinitely.

class Truthmachine {
    static function main(){
      var num = Sys.stdin().readLine();
      if (num == "0"){
        Sys.stdout().writeString("0");
      }else if (num == "1"){
        while (1 == 1){
          Sys.stdout().writeString("1");
        }
      } 
    }
}
