// This is the classic 99 bottles of beer program. I don't really need to explain this. This code is really, _really_ wonky.

class Bottlesofbeer {
    static function main(){
        for (i in -99...0){
            if (i != -1){
                Sys.println('${i*-1} bottles of beer on the wall, ${i*-1} bottles of beer.\nTake one down & pass it around, ${i*-1 -1} bottles of beer on the wall.');
            }else{
                Sys.println('1 bottle of beer on the wall, 1 bottle of beer.\nTake one down & pass it around, 1 bottle of beer on the wall.\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.');
            }
        }
    }
}
